export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const USER_UPDATE = 'user_update';
export const USER_CREATE = 'user_create';
export const USER_FETCH_SUCCESS = 'user_fetch_success';
export const USER_SAVE_SUCCESS = 'user_save_success';



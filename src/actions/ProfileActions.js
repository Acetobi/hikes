import firebase from 'firebase';
import {Actions} from 'react-native-router-flux';
import {
	USER_UPDATE,
	USER_CREATE,
	USER_FETCH_SUCCESS,
	USER_SAVE_SUCCESS
} from './types';

export const userUpdate = ({prop, value}) => {
	return{
		type: USER_UPDATE,
		payload: {prop, value}
		};
}

export const userProfile = ({name, phone, shift}) => {
	const {currentUser} = firebase.auth();
	
	return (dispatch) => {
		firebase.database().ref(`/users/${currentUser.uid}/employees`)
			.push({name, phone, shift})
			.then(() => {
				dispatch({type: USER_CREATE})
				Actions.pop({type: "reset"})
			});

	}
}

export const userFetch = () => {
	const {currentUser} = firebase.auth();

	return(dispatch) => {
		firebase.database().ref(`/users/${currentUser.uid}/employees`)
			.on('value', snapshot => {
				dispatch({type: USER_FETCH_SUCCESS, payload: snapshot.val()});
			});
	}
}

export const userSave = ({name, phone, shift, uid}) => {
	const {currentUser} = firebase.auth();

	return (dispatch) => {
		firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
			.set({name, phone, shift})
			.then(() => {
				dispatch({type: USER_SAVE_SUCCESS});
				Actions.main({type: "reset"})
			});
	}
}

export const userDelete = ({uid}) => {
	const {currentUser} = firebase.auth();

	return (dispatch) => {
		firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
			.remove()
			.then(() => {
				Actions.main({type: 'reset'})
			})
	}
}
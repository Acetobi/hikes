import React from 'react';
import {Scene, Router, Actions} from 'react-native-router-flux';
import LoginForm from './components/screens/LoginForm';
import Home from './components/screens/Home';
import Routes from './components/screens/Routes';
import UserProfile from './components/screens/UserProfile';
import UserEdit from './components/screens/UserEdit';
import Gallery from './components/screens/Gallery';

const RouterComponent = () => {
	return (
		<Router>

			<Scene key="root" hideNavBar>
				<Scene key="auth">
					<Scene
						key="login"
						component={LoginForm}
						title="Please Login"
					/>
				</Scene>
				<Scene key="main">
					<Scene
						rightTitle="Add+"
						onRight={() => {console.log('right!!!'), Actions.profileCreate()}}
						key="home"
						component={Home}
						title="Home"
						initial
					/>
					<Scene
						key="profileCreate"
						component={UserProfile}
						title="User Profile"
					/>
					<Scene
						key="profileEdit"
						component={UserEdit}
						title="Edit User Profile"
					/>
					<Scene 
						key="routes"
						component={Routes}
						title="Routes"
					/>
					<Scene 
						key="gallery"
						component={Gallery}
						title="Gallery"
					/>
				</Scene>	
			</Scene>

		</Router>
		)
};

export default RouterComponent;
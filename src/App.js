import React, {Component} from 'react';
import {ImageBackground, View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
// import LoginForm from './components/screens/LoginForm';
import Router from './Router'

class App extends Component {

	componentWillMount() {
		firebase.initializeApp({
    		apiKey: "AIzaSyDzDHuX5mhfdO7KnmlXRSCootEZil1atAQ",
    		authDomain: "hikes-8aa45.firebaseapp.com",
    		databaseURL: "https://hikes-8aa45.firebaseio.com",
    		projectId: "hikes-8aa45",
    		storageBucket: "hikes-8aa45.appspot.com",
    		messagingSenderId: "768637738139"
  		});

  		// firebase.auth().onAuthStateChanged((user) =>{
  		// 	if(user) {
  		// 		this.setState({loggedIn: true});
  		// 	} else {
  		// 		this.setState({loggedIn: false})
  		// 	}
  		// });
	}

	render() {
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
		return(
			<Provider store={store}>
				<Router />
			</Provider>

			)
	}
}

// const styles = {
// 	container: {
// 		flex: 1,
// 		width: undefined,
// 		height: undefined,
// 		backgroundColor: 'transparent',
// 		justifyContent: 'center',
// 		alignContent: 'center'
// 	}
// }

export default App;
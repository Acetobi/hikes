import React, {Component} from 'react';
import {ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import {emailChanged, passwordChanged, loginUser} from '../../actions';
import {Button, Card, CardSection, Input, Spinner} from '../common';

class LoginForm extends Component {
	// state = {email: '', password: '', error: '', loading: false};

	onButtonPress() {
		const {email, password} = this.props;

		this.props.loginUser({email, password});
	};
	onEmailChange(text){
		this.props.emailChanged(text);
	};

	onPasswordChange(text){
		this.props.passwordChanged(text);
	};

	renderButton() {
		if (this.props.loading) {
			return <Spinner size='large' />
		}

		return (
			<Button onPress={this.onButtonPress.bind(this)}>
				Login
			</Button>
			)
	}
	renderError() {
		if(this.props.error){
			return (
				<View style={{backgroundStyle: 'white'}}>
					<Text style={styles.errorStyle}>
						{this.props.error}
					</Text>
				</View>
				)
		}
	}

	render () {
		return (
		<ImageBackground
				source={require('../../Image/vectorstock_3840290.jpg')}
				style={styles.container}
			>
			<Card>
				<CardSection>
					<Input
						placeholder='Email'
						value={this.props.email}
						onChangeText={this.onEmailChange.bind(this)}
					/>
				</CardSection>

				<CardSection>
					<Input
						secureTextEntry
						placeholder='Password'
						value={this.props.password}
						onChangeText={this.onPasswordChange.bind(this)}
					/>
				</CardSection>
				
				<CardSection>
					{this.renderButton()}
				</CardSection>

				{this.renderError()}
			</Card>
			</ImageBackground>
		)
	}
}
const styles = {
	errorStyle: {
		fontSize: 30,
		color: 'red'
	},
	container: {
		flex: 1,
		width: undefined,
		height: undefined,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignContent: 'center'
	}
}

const mapStateToProps = ({auth}) => {
	const {email, password, error, loading} = auth;

	return {email, password, error, loading}
};

export default connect(mapStateToProps, {emailChanged, passwordChanged, loginUser})(LoginForm);
import React, {Component} from 'react';
import {View, Picker, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {userUpdate} from '../../actions';
import {CardSection, Input} from '../common';

class UserForm extends Component {
	render() {
		return(
			<View>
				<CardSection>
						<Input
							placeholder="Name"
							value={this.props.name}
							onChangeText={value => this.props.userUpdate({prop: 'name', value})}
						/>
					</CardSection>

					<CardSection>
						<Input
							placeholder="098-276-92-92"
							value={this.props.phone}
							onChangeText={value => this.props.userUpdate({prop: 'phone', value})}
						/>
					</CardSection>
						
						<View style={styles.viewStyle}>
						<Picker
							style={styles.pickerStyle}
							selectedValue={this.props.shift}
							onValueChange={day => this.props.userUpdate({prop: 'shift', value: day})}
						>
							<Picker.Item label="Monday" value="Monday" />
							<Picker.Item label="Thuesday" value="Thuesday" />
							<Picker.Item label="Wednesday" value="Wednesday" />
							<Picker.Item label="Thursday" value="Thursday" />
							<Picker.Item label="Friday" value="Friday" />
							<Picker.Item label="Saturday" value="Saturday" />
							<Picker.Item label="Sunday" value="Sunday" />
						</Picker>
						</View>
					
			</View>
		)
	};
};

const styles = {
	pickerStyle: {
		color: 'white',
		width: 150
	},
	viewStyle: {
		backgroundColor: 'rgba(0,0,0,.5)',
		paddingLeft: 130,
		top: -0.3
	}
};

const mapStateToProps = (state) => {
	const {name, phone, shift} = state.profileForm;

	return {name, phone, shift};
};

export default connect(mapStateToProps , {userUpdate})(UserForm);
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ImageBackground} from 'react-native'
import {userUpdate, userProfile} from '../../actions';
import {Button, CardSection} from '../common';
import UserForm from './UserForm';

class UserProfile extends Component {
	onButtonPress() {
		const {name, phone, shift} = this.props;

		this.props.userProfile({name, phone, shift: shift || 'Monday'});
	}
	render() {
		console.log(this.props.user);

		return(
			<ImageBackground
				source={require('../../Image/vectorstock_3840290.jpg')}
				style={styles.container}
			>
				
					<UserForm {...this.props}/>
					<CardSection>
						<Button onPress={this.onButtonPress.bind(this)}>
							Create
						</Button>
					</CardSection>
				
			</ImageBackground>
			)
	}
}

const styles = {
	container: {
		flex: 1,
		width: undefined,
		height: undefined,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignContent: 'center'
	}
}


const mapStateToProps = (state) => {
	const {name, phone, shift} = state.profileForm;

	return {name, phone, shift}
}

export default connect(mapStateToProps, {
	userUpdate,
	userProfile
})(UserProfile);
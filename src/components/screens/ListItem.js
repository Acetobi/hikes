import React, {Component} from 'react';
import {Text, TouchableWithoutFeedback, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {CardSection} from '../common';

class ListItem extends Component {
	onRowPress() {
		Actions.profileEdit({user: this.props.user});
	}
	render() {
		const {name} = this.props.user;

		return(
				<TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
						<View>
								<Text style={styles.titleStyle}>
									{name}
								</Text>
						</View>
				</TouchableWithoutFeedback>
			)
	}
}

const styles = {
	titleStyle: {
		fontSize: 24,
		marginTop: 10,
		textAlign: 'center'
	}
}

export default ListItem;
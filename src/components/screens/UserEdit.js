import _ from 'lodash';
import React, {Component} from 'react';
import {ImageBackground, View} from 'react-native';
import {connect} from 'react-redux';
import UserForm from './UserForm';
import {userUpdate, userSave, userDelete} from '../../actions';
import {CardSection, Button, Confirm} from '../common';

class UserEdit extends Component {
	state = {showModal: false};
	componentWillMount() {
		_.each(this.props.user, (value, prop) => {
			this.props.userUpdate({prop, value});
		}) 
	}

	onButtonPress() {
		const {name, phone, shift} = this.props;
		
		this.props.userSave({name, phone, shift, uid: this.props.user.uid})
	}

	onAccept() {
		const {uid} = this.props.user;

		this.props.userDelete({uid})
	}

	onDecline() {
		this.setState({showModal: false})
	}

	render() {
		return(
			<ImageBackground
				source={require('../../Image/vectorstock_3840290.jpg')}
				style={styles.container}
			>
			
			<UserForm />
				<View style={{top: -0.2}}>
					<CardSection>
						<Button onPress={this.onButtonPress.bind(this)}>
							Save Changes
						</Button>
					</CardSection>

					<CardSection>
						<Button onPress={() => this.setState({showModal: !this.state.showModal})}>
							Delete User
						</Button>
					</CardSection>
				</View>

				<Confirm
					visible={this.state.showModal}
					onAccept={this.onAccept.bind(this)}
					onDecline={this.onDecline.bind(this)}
				>
					Are you sure you want to delete?
				</Confirm>
			
			</ImageBackground>
			)
	}
}

const styles = {
	container: {
		flex: 1,
		width: undefined,
		height: undefined,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignContent: 'center'
	}
}

const mapStateToProps = (state) => {
	const {name, phone, shift} = state.profileForm;

	return {name, phone, shift};
}

export default connect(mapStateToProps, {
	userUpdate,
	userSave,
	userDelete
})(UserEdit);
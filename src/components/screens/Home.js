import _ from 'lodash'
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Router, Actions} from 'react-native-router-flux';
import {ListView, ImageBackground, View, Text} from 'react-native';
import {userFetch} from '../../actions';
import ListItem from './ListItem';
import {CardSection, Button} from '../common';

class Home extends Component {
	componentWillMount() {
		this.props.userFetch();

		this.createDataSource(this.props);
	}

	componentWillReceiveProps(nextProps) {
		// nextProps are the next set of props that this component
		//will be  rendered with
		// this.props is still the old set of props
		this.createDataSource(nextProps);
	}

	createDataSource({users}) {
		const ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2
		});

		this.dataSource = ds.cloneWithRows(users); 
	}

	renderRow(user) {
		return <ListItem user={user} />
	}
	render () {
		console.log(this.props)
		return (
			<ImageBackground
				source={require('../../Image/vectorstock_3840290.jpg')}
				style={styles.container}
			>
			
			<View style={styles.viewStyle}>
				<View>
					<ListView
						enableEmptySections
						dataSource={this.dataSource}
						renderRow={this.renderRow}
					/>
				</View>

				<View>
					<CardSection>
						<View style={styles.footerStyle}>
							<Button onPress={Actions.gallery}>Gallery</Button>
							<Text style={styles.textStyle}> / </Text>
							<Button onPress={Actions.routes}>Routes</Button>
						</View>
					</CardSection>
				</View>
			</View>

			</ImageBackground>
			)
	}
}

const styles = {
	container: {
		flex: 1,
		width: undefined,
		height: undefined,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignContent: 'center'
	},
	viewStyle: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'center',
		justifyContent: 'space-between'
	},
	footerStyle: {
		flex: 1,
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
		padding: 10
	},
	textStyle: {
		paddingTop: 10,
		paddingBottom: 10,
		fontSize: 18,
		color: 'white',
		justifyContent: 'center'
	}
}

const mapStateToProps = state => {
	const users = _.map(state.users, (val, uid) => {
		return {...val, uid};
	});

	return {users};
}

export default connect(mapStateToProps, {userFetch})(Home);
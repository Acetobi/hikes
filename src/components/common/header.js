import React from 'react';
import {Text, View} from 'react-native';

const Header = (props) => {
	const {textStyle, viewStyle} = styles;

	return (
		<View style={viewStyle}>
			<Text style={textStyle}>{props.headerText}</Text>
		</View>
		)
}

const styles = {
	viewStyle: {
		// flex: 1,
		// backgroundColor: 'transparent',
		backgroundColor: 'rgba(0,0,0,.2)',
		justifyContent: 'center',
		alignItems: 'center',
		height: 80
	},
	textStyle: {
		fontSize: 30,
		fontWeight: '900',
		color: 'white'
	}
}
export {Header};
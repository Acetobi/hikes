import React from 'react';
import {View, Dimensions} from 'react-native';

const CardSection = (props) => {
	return (
		<View style={styles.containerStyle}>
			{props.children}
		</View>
		);
};

const styles = {
	containerStyle: {
		// // borderBottomWidth: 1,
		// flex: 1,
		// padding: 5,
		// width: 100,
		// height: 20,
		// backgroundColor: 'rgba(0,0,0,.1)',
		// // justifyContent: 'flex-start',
		// // flexDirection: 'row',
		// borderColor: 'red',
		// borderWidth: 1,
		// flexDirection: 'column',
		// justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(0,0,0,.5)',
		// // position: 'relative'
		width: Dimensions.get('window').width,
		height: 50,
	}
};

export {CardSection}; 
import React, {Component} from 'react';
import {Modal, Text, View} from 'react-native';
import {CardSection} from './CardSection';
import {Button} from './Button';

const Confirm = ({children, visible, onAccept, onDecline}) => {
  const {cardSectionStyle, textStyle, containerStyle} = styles;
  return(
    <Modal
      visible={visible}
      transparent
      animationType="slide"
      onRequestClose={() => {}}
    >
      <View style={containerStyle}>
        <CardSection style={cardSectionStyle}>
          <Text style={textStyle}>{children}</Text>
        </CardSection>

        <CardSection>
          <Button onPress={onAccept}>Accept</Button>
        </CardSection>
        <CardSection>
          <Button onPress={onDecline}>Decline</Button>
        </CardSection>
      </View>
    </Modal>
    )
}
const styles = {
  cardSectionStyle: {
    justifyContent: 'center'
  },
  textStyle: {
    flex: 1,
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
    lineHeight: 40,
    borderBottomWidth: 1,
    borderColor: 'white'
  },
  containerStyle: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
}

export {Confirm};
import React from 'react';
import {View, TextInput} from 'react-native';

const Input = ({value, onChangeText, placeholder, secureTextEntry}) => {
	const {inputStyle, containerStyle} = styles;
	return (
		<View style={containerStyle}>
			<TextInput
				secureTextEntry={secureTextEntry}
				placeholder={placeholder}
				placeholderTextColor='white'
				autoCorrect={false}
				style={inputStyle}
				value={value}
				onChangeText={onChangeText}
				underlineColorAndroid='rgba(0,0,0,0)'
			/>
		</View>
	)
}

const styles = {
	inputStyle: {
		width: 300,
		height: 50,
		color: 'white',
		paddingRight: 5,
		paddingLeft: 5,
		fontSize: 22,
		justifyContent: 'center',
		// alignItems: 'center',
		textAlign: 'center',
		lineHeight: 30,
		// backgroundColor: 'transparent'
	},
	containerStyle: {
		// backgroundColor: 'red',
		// width: 200,
		// height: 40,
		// flex: 1,
		// alignContent: 'center'
	}
}

export {Input};
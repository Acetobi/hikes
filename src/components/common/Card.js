import React from 'react';
import {View} from 'react-native';

const Card = (props) => {
	return (
		<View style={styles.containerStyle}>
			{props.children}
		</View>
		);
};

const styles = {
	containerStyle: {
		// // flexDirection: 'column',
		// // justifyContent: 'center',
		// // alignItems: 'center',
		// borderWidth: 2,
		// borderColor: 'red',
		// // borderBottomWidth: 0,
		// backgroundColor: 'red',
		// // elevation: 1,
		// marginTop: 5,
		// width: 300,
		// height: 500
		flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'

	}
};

export {Card}; 
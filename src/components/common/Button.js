import React from 'react';
import {Text, TouchableOpacity} from 'react-native';


const Button = ({ onPress, children}) => {
	const {buttonStyle, textStyle} = styles;

	return (
		<TouchableOpacity onPress={onPress} style={buttonStyle}>
			<Text style={textStyle}>
				{children}
			</Text>
		</TouchableOpacity> 
		);
};

const styles = {
	textStyle: {
		alignSelf: 'center',
		fontSize: 16,
		fontWeight: '600',
		letterSpacing: 6,
		paddingTop: 10,
		paddingBottom: 10,
		color: 'white'
	},
	buttonStyle: {
		// flex: 1,
		alignSelf: 'center',
		backgroundColor: 'transparent',
		// borderRadius: 5,
		// borderWidth: 1,
		marginLeft: 10,
		marginRight: 10,
	}
};

export {Button};
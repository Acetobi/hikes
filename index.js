import { AppRegistry, YellowBox } from 'react-native';
import App from './src/App';

console.disableYellowBox = true;
YellowBox.ignoreWarnings([
'Remote debugger is in a background tab which may cause apps to perform slowly. Fix this by foregrounding the tab (or opening it in a separate window).',
'Warning: isMounted(...) is deprecated in plain JavaScript React classes. Instead, make sure to clean up subscriptions and pending requests in componentWillUnmount to prevent memory leaks.'
	]);

AppRegistry.registerComponent('HikesUpdate', () => App);
